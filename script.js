function solveHanoi(n, a, b, c, d, e, f) {
  // n = number of disks, a = source, b = auxiliary, c = destination
  if (n === 1) {
    console.log(`${a} -> ${c}`);
  } else {
    solveHanoi(n - 1, a, c, b);
    console.log(`${a} -> ${c}`);
    solveHanoi(n - 1, b, a, c);
  }
}
console.log(solveHanoi(3, "a", "b", "c"));
